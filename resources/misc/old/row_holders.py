# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
class BytesRowHolder(object):
    """
    A container class with the job of holding a row of data and a buffer
    counter that will keep a live count of the numbers of rows of data derived
    from the input file that are held in the buffer at any one time. The
    buffer_counter links the row to coming from a particular file. That way
    when the buffer counter is at zero we know that we need to load more rows
    from that file. This also provides item access to the chromosome name,
    start position and end position columns in the row. These are used by
    `operator.itemgetter`.
    """
    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def __init__(self, row, buffer_counter):
        """
        Parameters
        ----------
        row : list of bytes or int
            The row from the file contained in the buffer counter
        buffer_counter : :obj:`BufferCounter`
            A buffer counter that is "associated" with the row
        """
        # Set up the datatypes for the row
        for i in buffer_counter.keys:
            row[i[0]] if row[i[1]] in [str, bytes] else i[1](row[i[0]])

        self.raw_row = row
        self.buffer_counter = buffer_counter

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    @property
    def row(self):
        """
        Return the row after converting back to str or bytes
        """
        row = self.raw_row
        for i in self.buffer_counter.keys:
            row[i[0]] if isinstance(row[i[0]], bytes) else str(row[i[0]]).encode()
        return row

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def __repr__(self):
        """
        Output the chr_name, start_pos and end_pos fields from the row
        """
        return '[{0}]'.format(
            ", ".join([str(self.row[i[0]]) for i in self.buffer_counter.keys])
        )

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def __getitem__(self, idx):
        """
        Return the element from the row at idx

        Returns
        -------
        index_element:
            The row element at idx
        """
        return self.raw_row[idx]


# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
class StrRowHolder(object):
    """
    A container class with the job of holding a row of data and a buffer
    counter that will keep a live count of the numbers of rows of data derived
    from the input file that are held in the buffer at any one time. The
    buffer_counter links the row to coming from a particular file. That way
    when the buffer counter is at zero we know that we need to load more rows
    from that file. This also provides item access to the chromosome name,
    start position and end position columns in the row. These are used by
    `operator.itemgetter`.
    """
    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def __init__(self, row, buffer_counter):
        """
        Parameters
        ----------
        row : list of bytes or int
            The row from the file contained in the buffer counter
        buffer_counter : :obj:`BufferCounter`
            A buffer counter that is "associated" with the row
        """
        # pp.pprint(buffer_counter.keys)
        # Set up the datatypes for the row
        for i in buffer_counter.keys:
            row[i[0]] if i[1] in [str, bytes] else i[1](row[i[0]])

        self.raw_row = row
        self.buffer_counter = buffer_counter

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def row(self):
        """
        Return the row after converting back to str or bytes
        """
        row = self.raw_row
        for i in self.buffer_counter.keys:
            row[i[0]] if isinstance(row[i[0]], str) else str(row[i[0]])
        return row

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def __repr__(self):
        """
        Output the chr_name, start_pos and end_pos fields from the row
        """
        return '[{0}]'.format(
            ", ".join([str(self.row[i[0]]) for i in self.buffer_counter.keys])
        )

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def __getitem__(self, idx):
        """
        Return the element from the row at idx

        Returns
        -------
        index_element:
            The row element at idx
        """
        return self.raw_row[idx]


