"""
Prototype external merge sort algorithm written in Python
"""
import pandas as pd
import tempfile
import os
import shutil
import csv
import gzip
import pprint as pp
from multiprocessing import Pool

_ALLOWED_PANDAS = [
    'delimiter',
    'skiprows',
    'skipfooter',
    'nrows',
    'skip_blank_lines',
    'compression',
    'quotechar',
    'quoting',
    'escapechar',
    'comment',
    'encoding',
    'dialect'
]

_PANDAS_TO_CSV = [
    'quotechar',
    'quoting',
    'escapechar',
    'encoding',
]


# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
class ChunkMergeSort(object):
    """
    Given a set of chunks this will iterate over their contents and provide
    rows in the sort order defined by sort_keys
    """
    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def __init__(self, chunks, sort_keys, chunksize=10000, use_dtypes=False,
                 delete=True, **kwargs):
        """
        Parameters
        ----------
        chunks : list of str
            paths to the chunk files
        sort_keys : list of tuple
            The columns and order, data_type of the sort. Each tuple can have
            either 2 or 3 elements. [0] is the column name of each chunk that
            needs to be sorted on. [1] is the data type of the sort column. The
            optional [2] is the sort order. If provided it should be a Boolean
            where True means sort in ascending order and False descending order
            If absent then the sort defaults to True (ascending).
        chunksize : int, optional, default: 10000
            The number of rows from each chunk to load into memory at any one
            time
        use_dtypes : bool, optional, default: False
            Allow pandas to infer the data types of non-sort-key columns. False
            means no (all will be str), True means yes
        delete : bool, optional, default: True
            Delete chunks as they are processed and/or if this should error out
        **kwargs
            Other arguments that are passed through to `pandas.read_csv`. These
            are `delimiter`, `skiprows`, `skipfooter`, `nrows`,
            `skip_blank_lines`, `compression`, `quotechar`, `quoting`,
            `escapechar`, `comment`, `encoding`, `dialect`

        Raises
        ------
        ValueError
            If the number of chunks is <= 1
        """
        if len(chunks) <= 1:
            raise ValueError("no chunks to merge")
        _check_pandas_kwargs(**kwargs)

        self._chunks = chunks
        self._sort_keys = sort_keys
        self._chunksize = chunksize
        self._delete = delete
        self._kwargs = kwargs
        self._use_dtypes = use_dtypes
        self._sort_by, self._order_by = format_sort_keys(self._sort_keys)

        # All the chunks should have the same header, so just extract the
        # header from the first chunk to represent all of them
        self.header = []
        self._chunk_df = []
        self._chunk_counts = {}
        self._sorted_chunk = pd.DataFrame()
        self._row_idx = 0

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def __enter__(self):
        """
        Enter the context manager
        """
        self.open()
        return self

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def __exit__(self, *args):
        """
        Exit the scope of the context manager
        """
        self.close()

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def open(self):
        """
        Open all the chunks and initialise the data and the header
        """
        self.header = extract_header(self._chunks[0], **self._kwargs)
        self._define_track_column()

        default_dtype = str
        if self._use_dtypes is True:
            default_dtype = None

        self._dtypes = get_dtypes(
            self.header,
            self._sort_keys,
            default=default_dtype
        )

        self._chunk_df = [
            pd.read_csv(
                i,
                chunksize=self._chunksize,
                dtype=self._dtypes,
                **self._kwargs
            ) for i in self._chunks
        ]
        self._chunk_counts = dict(
            [(idx, 0) for idx in range(len(self._chunk_df))]
        )

        # Pull data from all the chunks
        self._merge_and_sort(
            [self._read_chunk(idx) for idx in range(len(self._chunk_df))]
        )

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def close(self, delete=True):
        """
        Close all files and delete
        """
        # close and delete the chunks
        if delete is True:
            for i in range(len(self._chunks)):
                try:
                    # Attempt to delete the chunk files, they should have been
                    # deleted if they have been processed but
                    self._delete_chunk(i)
                except FileNotFoundError:
                    pass

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def _define_track_column(self):
        """
        Each chunk will be given a tracking column which determines the file
        that the chunk has come from. This allows us to determine when we need
        to load the next batch of rows from that chunk. This is called a
        tracking column and this method makes sure that it is not named after
        any columns that already exist in the data
        """
        self.track_column = "source_idx"
        idx = 1
        while self.track_column in self.header:
            self.track_column = "{0}{1}".format(
                self.track_column,
                idx
            )
            idx += 1

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def _read_chunk(self, idx):
        """
        Read a chunk of data (of chunksize rows) from the chunk file at
        position idx

        Parameters
        ----------
        idx : int
            The chunk index to read the next batch of data from

        Returns
        -------
        chunk : :obj:`pandas.DataFrame`
            A data frame containing at most chunksize rows

        Raises
        ------
        StopIteration
            If all the chunks have been exhausted
        """
        try:
            chunk = next(self._chunk_df[idx])
            self._chunk_counts[idx] = chunk.shape[0]
        except StopIteration:
            # delete from counts
            del self._chunk_counts[idx]

            # delete the chunk file
            self._delete_chunk(idx)

            if len(self._chunk_counts) == 0:
                raise
            else:
                raise IndexError("chunk empty")

        chunk.index = pd.MultiIndex.from_product([chunk.index, [idx]])
        return chunk

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def _delete_chunk(self, idx):
        """
        closes and deletes the chunk file

        Parameters
        ----------
        idx : int
            The chunk index to read the next batch of data from
        """
        self._chunk_df[idx].close()

        if self._delete is True:
            os.unlink(self._chunks[idx])

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def _merge_and_sort(self, chunks):
        """
        Concatenate all the chunks and sort by the required sort keys

        Parameters
        ----------
        chunks : list of :obj:`pandas.DataFrame`
            The chunks to join together and sort
        """
        self._sorted_chunk = pd.concat(chunks).sort_values(
            self._sort_by,
            axis=0,
            ascending=self._order_by,
            ignore_index=False
        )

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def __iter__(self):
        """
        The entry point for the iterator
        """
        return self

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def __next__(self):
        """
        Get the next row in the sort order

        Returns
        -------
        row : :obj:`pandas.Series`
            The next row in the sort order

        Raises
        ------
        StopIteration
            When there are no more rows to return
        """
        # The next row in sort order
        try:
            next_row = self._sorted_chunk.iloc[self._row_idx]
            self._row_idx += 1
        except IndexError:
            raise StopIteration("no more data")

        # The chunk index of that row
        file_idx = next_row.name[1]

        # Track how many rows for that dataset are remaining
        self._chunk_counts[file_idx] -= 1

        # if there are no rows for that dataset remaining then we load up the
        # next batch of rows for that dataset
        if self._chunk_counts[file_idx] == 0:
            self._sorted_chunk.drop(
                self._sorted_chunk.index[:self._row_idx],
                inplace=True
            )

            self._row_idx = 0
            try:
                self._merge_and_sort(
                    [
                        self._sorted_chunk,
                        self._read_chunk(file_idx)
                    ]
                )
            except (StopIteration, IndexError):
                pass
        return next_row


# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
class ChunkMergeBatchSort(object):
    """
    Merge all the chunks and provide an iterator to the sorted rows
    TODO: un-finished
    """
    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    @classmethod
    def get_batches(cls, chunks, batch_size):
        """
        Return a list containing n lists containing a max of batch_size
        elements

        Parameters
        ----------
        chunks : list
            A list of file names to split into batches
        batch_size : int
            The max number of file names in each batch

        Returns
        -------
        batches : list of lists
            A list of filename batches (sub lists), each sub list will have a
            max of batch_size elements
        """
        return [chunks[i:i + batch_size]
                for i in range(0, len(chunks), batch_size)]

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def __init__(self, chunks, sort_keys, dir=tempfile.mkdtemp(),
                 chunksize=100000, max_files=5, processes=1, delimiter="\t"):
        self._chunks = chunks
        self._max_files = max(int(max_files/processes), 2)
        self._tmp_dir = dir
        self._sort_keys = sort_keys
        self._chunksize = max(int((chunksize/max_files)/processes), 10)
        self._processes = processes
        self._delimiter = delimiter
        self._header = extract_header(chunks[0], delimiter)
        self._final_chunks = []

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def __iter__(self):
        """
        """
        self._process_chunks()
        self._chunk_reader = ChunkReader(
            self._final_chunks,
            self._sort_keys,
            chunksize=self._chunksize,
            delimiter=self._delimiter,
            use_dtypes=False
        )
        self._chunk_reader.open()
        return self

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def __next__(self):
        """
        """
        return next(self._chunk_reader)

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def _process_chunks(self):
        """
        """
        if len(self._chunks) <= self._max_files:
            self._final_chunks = self._chunks
        elif self._processes > 1:
            self._multi_process()
        else:
            self._single_process()

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def _multi_process(self):
        """
        """
        self._batches = self.__class__.get_batches(
            self._chunks, self._max_files
        )
        pp.pprint(self._batches)
        self._next_chunks = []
        while len(self._batches) > 1:
            temp_files = [make_temp(dir=self._tmp_dir, suffix=".gz") for i in self._batches]
            with Pool(processes=self._processes) as pool:
                job_args = [
                    (
                        temp_files[i],
                        self._batches[i],
                        self._sort_keys,
                        self._chunksize,
                        self._delimiter
                    ) for i in range(len(self._batches))
                ]
                try:
                    self._next_chunks = pool.map(process_chunks, tuple(job_args))
                except Exception:
                    for i in temp_files:
                        os.unlink(i)
                    raise

                self._batches = self.__class__.get_batches(
                    self._next_chunks, self._max_files
                )
        self._final_chunks = self._batches[0]

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def _single_process(self):
        """
        """
        self._batches = self.__class__.get_batches(
            self._chunks, self._max_files
        )

        self._next_chunks = []
        while len(self._batches) > 1:
            for chunks in self._batches:
                self._next_chunks.append(
                    process_chunks(
                        (
                            make_temp(dir=self._tmp_dir),
                            chunks,
                            self._sort_keys,
                            self._chunksize,
                            self._delimiter
                        )
                    )
                )
            self._batches = self.__class__.get_batches(
                self._next_chunks, self._max_files
            )
        self._final_chunks = self._batches[0]


# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
# class ExtMergeSort(ChunkMergeBatchSort):
#     """
#     TODO: Untested
#     """
#     # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
#     def __init__(self, infile, **kwargs):
#         """

#         """
#         pass


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def write_chunks(infile, sort_keys, chunksize=100000, dir=tempfile.mkdtemp(),
                 use_gzip=True, **kwargs):
    """
    Read the input file and write sorted files (chunks) of chunksize rows to
    the dir. The input file must have a header row.

    Parameters
    ----------
    infile : str
        The input file name
    sort_keys : list of tuple
        The columns and data_type and order of the sort. Each tuple can have
        either 2 or 3 elements. [0] is the column name of each chunk that
        needs to be sorted on. [1] is the data type of the sort column. The
        optional [2] is the sort order. If provided it should be a Boolean
        where True means sort in ascending order and False descending order
        If absent then the sort defaults to True (ascending).
    chunksize : int, optional, default: 100000
        The max number of rows in each pre-sorted chunk file
    dir : str, optional, default: tempdir
        A directory to write the chunks to if not provided then a temp
        directory is created in the system temp
    use_gzip : bool, optional, default: True
        Write the chunks as gzip compressed files. May be useful if you have
        limited temp space
    **kwargs
        Other arguments that are passed through to `pandas.read_csv`. These are
        `delimiter`, `skiprows`, `skipfooter`, `nrows`, `skip_blank_lines`,
        `compression` (applies to reading only), `quotechar`, `quoting`,
        `escapechar`, `comment`, `encoding`, `dialect`
    """
    _check_pandas_kwargs(**kwargs)
    header = extract_header(infile, **kwargs)
    dtypes = get_dtypes(header, sort_keys, default=str)

    idx = 1
    chunk_iter = pd.read_csv(
        infile,
        dtype=dtypes,
        chunksize=chunksize,
        **kwargs
    )

    # Split up the sort keys so they are compatible with Pandas sort_values
    sort_by, order_by = format_sort_keys(sort_keys)
    to_csv_kwargs = _get_pandas_to_csv_kwargs(use_gzip=use_gzip, **kwargs)

    chunk_files = []
    for chunk in chunk_iter:
        outfile = make_temp(dir=dir)
        chunk.sort_values(
            sort_by,
            axis=0,
            ascending=order_by,
            ignore_index=True,
            inplace=True
        )
        chunk.to_csv(outfile, index=False, header=True, **to_csv_kwargs)
        chunk_files.append(outfile)
        idx += 1
    return chunk_files


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def _check_pandas_kwargs(**kwargs):
    """
    Error check that pandas kwargs.

    Parameters
    ----------
    **kwargs
        Other arguments that are passed through to `pandas.read_csv`. These are
        `delimiter`, `skiprows`, `skipfooter`, `nrows`, `skip_blank_lines`,
        `compression` (applies to reading only), `quotechar`, `quoting`,
        `escapechar`, `comment`, `encoding`, `dialect`

    Raises
    ------
    ValueError
        If any unexpected kwargs exist
    """
    for i in kwargs.keys():
        if i not in _ALLOWED_PANDAS:
            raise ValueError(
                "Not an allowed pandas keyword argument: {0}".format(i)
            )


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def _get_pandas_to_csv_kwargs(use_gzip=False, **kwargs):
    """
    Filter the pandas read kwargs and extract (into a new dict) those args
    appropriate for writing to a csv file.

    Parameters
    ----------
    use_gzip : bool, optional, default: True
        Any compression argument will be ignored, instead if this is `True`,
        then `compression='gzip'` will be applied. If `False` then
        `compression=None` will be applied
    **kwargs
        Arguments that are passed through to `pandas.read_csv`. Of these, only
        `delimiter` (changed to `sep`), `quotechar`, `quoting`, `escapechar`,
        `encoding` are kept.
    """
    to_csv = {}
    for i in _PANDAS_TO_CSV:
        try:
            to_csv[i] = kwargs[i]
        except KeyError:
            pass

    try:
        to_csv['sep'] = kwargs['delimiter']
    except KeyError:
        pass

    if use_gzip is True:
        to_csv['compression'] = 'gzip'
    else:
        to_csv['compression'] = None
    return to_csv


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def get_dtypes(columns, sort_keys, default=None):
    """
    Get a dictionary of column to data type mappings that will be passed to
    `pandas.read_csv()`

    Parameters
    ----------
    columns : list or str
        The column names of the data
    sort_keys : list of tuple
        The columns and order, data_type of the sort. Each tuple can have
        either 2 or 3 elements. [0] is the column name of each chunk that
        needs to be sorted on. [1] is the data type of the sort column. The
        optional [2] is the sort order. If provided it should be a Boolean
        where True means sort in ascending order and False descending order
        If absent then the sort defaults to True (ascending).
    default : NoneType or type
        The default data type of the non-sort key columns. If NoneType then
        pandas will infer the data types, otherwise the type supplied to
        default will be used. For example set to `str` to make sure no data
        type inference is applied to non-sort-key columns

    Returns
    -------
    dtypes : dict
        The data types to be supplied to the `dtype` parameter of
        `pandas.read_csv`
    """
    dtypes = {}

    if default is not None:
        dtypes = dict([(c, default) for c in columns])

        for i in sort_keys:
            dtypes[i[0]] = i[1]
    return dtypes


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def format_sort_keys(sort_keys):
    """
    Reformat the sort keys for arguments to Pandas.sort_values(). This turns
    the sort keys tuple into two lists that will be passed to the by and
    ascending parameters of Pandas.sort_values()

    Parameters
    ----------
    sort_keys : list of tuple
        The columns and order, data_type of the sort. Each tuple can have
        either 2 or 3 elements. [0] is the column name of each chunk that
        needs to be sorted on. [1] is the data type of the sort column. The
        optional [2] is the sort order. If provided it should be a Boolean
        where True means sort in ascending order and False descending order
        If absent then the sort defaults to True (ascending).

    Returns
    -------
    sort_by : list of str
        The column names that the data frame will be sorted on
    order_by : list of bool
        will be the same length as sort_by and indicates if the equivalent
        column in sort_by will be sorted ascending (True) or descending (False)
    """
    sort_by = []
    order_by = []
    for i in sort_keys:
        try:
            acsending = i[2]
        except IndexError:
            acsending = True
        sort_by.append(i[0])
        order_by.append(acsending)

    return sort_by, order_by


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def extract_header(infile, **kwargs):
    """
    Extract the header from the datafile using all the user defined pandas
    keyword arguments except for iterator or chunk size

    Parameters
    ----------
    infile : str
        The file with the header we want to extract
    **kwargs
        Other arguments that are passed through to `pandas.read_csv`. These are
        `delimiter`, `skiprows`, `skipfooter`, `skip_blank_lines`,
        `compression` (applies to reading only), `quotechar`, `quoting`,
        `escapechar`, `comment`, `encoding`, `dialect`

    Returns
    -------
    colnames : :obj:`pandas.Index`
        The header column names
    """
    _check_pandas_kwargs(**kwargs)
    kwargs['nrows'] = 0

    # We just want the header so do not read in any more than we need to
    df = pd.read_csv(infile, **kwargs)
    return df.columns


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def make_temp(**kwargs):
    """
    """
    tfh, tfn = tempfile.mkstemp(**kwargs)
    os.close(tfh)
    return tfn


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def process_chunks(args):
    outfile, chunks, sort_keys, chunksize, delimiter = args

    if len(chunks) == 1:
        shutil.copy2(chunks[0], outfile)
        return outfile
    with gzip.open(outfile, 'wt') as csvout:
        writer = csv.writer(csvout, delimiter=delimiter)
        with ChunkMergeSort(chunks, sort_keys, chunksize=chunksize,
                            delimiter=delimiter) as chunk_sort:
            writer.writerow(chunk_sort.header)
            for row in chunk_sort:
                # try:
                writer.writerow(row.to_list())
                # except UnicodeDecodeError:
                #     print("UnicodeErrorRaised in {0}".format(outfile))
    return outfile
