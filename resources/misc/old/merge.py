"""
Merge sort
"""
from merge_sort import row_holders, common
from operator import itemgetter
import os
import gzip
import pprint as pp


# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
class MergeFiles(object):
    """
    Performs a merge sort over many files
    """
    TEMP_PREFIX = "merge_sort"

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def __init__(self, files, keys, max_files=16, chunksize=100,
                 delimiter="\t", lineterminator="\n", tmp_dir=None,
                 open_method=gzip.open, header=True, row_holder=None,
                 as_bytes=False, **kwargs):
        """
        Parameters
        ----------
        files : str
            One or more file names. The first row of each file must be the
            header.
        keys : list of tuple
            The sort keys, each tuple should have column_name at [0],
            data_type at [1] and order at [2].
        max_files : int, optional, default: 16
            The maximum number of files that can be open (and merged) at any
            one time. The default (16) is the same as unix sort.
        chunksize : int, optional, default: 75
            The number of rows from each opened file that are buffered into
            memory.
        delimiter : str, optional, default: `\t`
            The delimiter of the input files (if un-opened). Note that so
            special allowance is given for quoted delimiters, so pass a
            csv.reader if you want them handled properly.
        lineterminator : str, optional, default: "\n"
            The line endings for each row
        tmp_dir : The directory for temp merge files to be placed if the
            merge can't be performed in a single pass.
        open_method : :obj:`func`, optional, default: `gzip.open`
            The method to use to open the input files if strings are given.
            Note, this defaults to gzip as presumably the reason you are using
            an external merge sort is the files are big.
        header : bool, optional, default: True
            Do the input files have a header, if so, this is extracted and
            checked that the length is equivalent for each input file.
        row_holder : :obj:`class`, optional, default: RowHolder
            A custom row holder that can be used to wrap the row and link it
            with the buffer counter. Must have a __getitem__ method and
            accept the row and buffer_counter object. The default one will
            work in all instances and casts the keys in the row to the correct
            datatype as defined by the key. A custom one may be able to do this
            more efficiently.
        **kwargs
            Keyword arguments to the open method
        """
        self._files = files
        self._keys = keys
        self._max_files = max_files
        self._chunksize = chunksize
        self._delimiter = delimiter
        self._lineterminator = lineterminator
        self._dir = tmp_dir
        self._open_method = open_method
        self._final = None
        self._temp_prefix = self.__class__.TEMP_PREFIX
        self._calls = 0
        self._tempfiles = []
        self._kwargs = kwargs
        self._has_header = header
        self._row_holder = row_holder
        self._as_bytes = as_bytes

        if self._as_bytes is True:
            self._open_mode = 'rb'
            self._delimiter = self._delimiter.encode(),
            self._lineterminator = self._lineterminator.encode()
            if row_holder is None:
                self._row_holder = row_holders.BytesRowHolder
        else:
            self._open_mode = 'rt'
            if row_holder is None:
                self._row_holder = row_holders.StrRowHolder

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def __enter__(self):
        """
        Entry point for the context manager
        """
        self.open()
        return self

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def __exit__(self, *args):
        """
        Exit point for the context manager
        """
        self.close()

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def __iter__(self):
        """
        Initialise the iterator
        """
        return self

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def __next__(self):
        """
        Grab the next row, rows are returned in sort order of chr_name,
        start_pos, end_pos

        Returns
        -------
        row : list of int or bytes
            All the entries are assumed to be bytes apart from the
            start_pos and end_pos that are made into ints.
        """
        try:
            row = next(self._final)
        except TypeError as e:
            if e.args[0] != "'NoneType' object is not an iterator":
                raise

            # Otherwise setup the sort
            self._sort()
            row = next(self._final)
        # print("MergeSort CALLS {0}".format(self._calls))
        # self._calls += 1
        return row

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def open(self):
        """
        Open all the files that need merging. Note that files that fail to open
        due to IOErrors do not cause fatal errors, rather they are added to an
        unprocessed list. This will only fail if < 2 files have been opened.
        The reason for this is that could run out of inodes if merging a lot
        of files and we do not want to crash the hold process but rather handle
        it.

        Raises
        ------
        IOError
            If less than 2 files are available to merge
        """
        header_len = -1
        header = None

        if self._has_header is True:
            # make sure all the files can be opened and initialise the header
            for i in self._files:
                with self._open_method(i, self._open_mode, **self._kwargs) as infile:
                    header = next(infile).strip(self._lineterminator).\
                        split(self._delimiter)

                    if header_len != len(header):
                        if header_len != -1:
                            raise(ValueError("header lengths not equal"))

                # Update the header length
                header_len = len(header)
        else:
            # If we are not checking the header just make sure we can open
            for i in self._files:
                open(i).close()

        self.header_len = header_len
        self.header = header

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def _sort(self):
        """
        Sort the contents I sort in reverse as I am poping from the bottom of
        the list to return
        """
        # This will hold all the files that need merging over the various
        # iterations
        self._remaining_files = list(self._files)

        # The arguments to MergeFiles, basically passing through from this
        # object
        kwargs = dict(
            chunksize=self._chunksize,
            delimiter=self._delimiter,
            lineterminator=self._lineterminator,
            header=self._has_header,
            open_method=self._open_method,
            row_holder=self._row_holder,
            as_bytes=self._as_bytes
        )
        kwargs = {**kwargs, **self._kwargs}

        # Holds the file names on intermediate (so files that are created
        # because we can't merge all the files in one go). These will be
        # deleted irrespective of the overall delete parameter
        self._tempfiles = []

        # A loop counter should I need it - although I will probably get
        # rid of this
        loop = 1

        # We merge until we get below are our file limit and then hopefully
        # we will have a one final merge
        while len(self._remaining_files) > self._max_files:
            # print("LOOP {0}".format(loop))

            # Will hold all the files that will be merged in this loop
            to_merge = []
            for i in range(self._max_files):
                try:
                    # Fill up from the start of the file list
                    to_merge.append(self._remaining_files.pop(0))
                except IndexError:
                    # No more files remaining
                    pass

            # This is to ensure that we never have a solitary remaining file
            if (len(self._remaining_files) + 1) == 1:
                # If we would have one but can't pull one back from what
                # we want to merge as that would make it is single file
                # then we add one from the remaining files
                if len(to_merge) == 2:
                    to_merge.append(self._remaining_files.pop())
                else:
                    self._remaining_files.append(to_merge.pop())

            # This will hold the sorted output for the current loop
            tmp_merge = common.get_tmp_file(
                dir=self._dir, prefix=self._temp_prefix
            )
            self._tempfiles.append(tmp_merge)

            # Now we merge into the temp file
            with BufferMerge(to_merge, self._keys, **kwargs) as merge:
                with self._open_method(
                        tmp_merge, self._open_mode, **self._kwargs
                ) as outfile:
                    if self._has_header is True:
                        outfile.write(
                            "{0}\n".format(self._delimiter.join(merge.header))
                        )
                    for row in merge:
                        outfile.write(
                            "{0}\n".format(self._delimiter.join(row.row()))
                        )
                # There is a possibility that MergeFiles could not open
                # all the files it wanted to - if that is the case then
                # they get added back to our remaining files
                self._remaining_files.extend(merge.unprocessed_files)

            # Also the tmp_file we just created is now remaining
            self._remaining_files.append(tmp_merge)

            # print("TO MERGE - LOOP {0}".format(loop))
            # pp.pprint(to_merge)
            # print("REMAINING - LOOP {0}".format(loop))
            # pp.pprint(self._remaining_files)
            loop += 1

        # So whilst we should be able to merge everything in a final run
        # there is a possibility that MergeFiles can't open all the files
        # because of inodes etc... So the while loop allows for that
        # possibility

        # We use a loop counter to ensure that we do not get stuck in an
        # infinite loop should we have a bad file that we can't merge
        loop = 1
        while len(self._remaining_files) > 0:
            # print("FINAL LOOP {0}".format(loop))
            # A fail safe to avoid infinite loops
            if loop == 3:
                raise IOError("unable to merge files")

            # We do not use a context manager here as we want to store it
            # so the Iterator can use it to return rows
            merge = Merge(list(self._remaining_files), self._keys, **kwargs)
            merge.open()
            # pp.pprint("BUFFER")
            # pp.pprint(merge.buffer[-1][:3])
            # print(len(merge.buffer))
            # After we have openned the MergeFiles object we can tell if it has
            # any files it has not been able to open, if so then we will have
            # to use another loop
            if len(merge.unprocessed_files) > 0:
                try:
                    self._remaining_files = list(merge.unprocessed_files)
                    tmp_merge = common.get_tmp_file(
                        dir=self._dir, prefix=self._tmp_prefix
                    )
                    self._tempfiles.append(tmp_merge)
                    with self._open_method(
                            tmp_merge, self._open_mode, **self._kwargs
                    ) as outfile:
                        if self._has_header is True:
                            outfile.write(
                                "{0}\n".format(
                                    self._delimiter.join(merge.header)
                                )
                            )
                        for row in merge:
                            outfile.write(
                                "{0}\n".format(self._delimiter.join(row.row()))
                            )
                    self._remaining_files.append(tmp_merge)
                finally:
                    # As we are not using a context manager, ensure the merge
                    # files object is closed no matter what happens
                    merge.close()
            else:
                # Process through the iterator
                self._final = merge
                self._remaining_files = []
                break
            loop += 1

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def close(self):
        """
        Close all the files
        """
        try:
            self._final.close()

        except AttributeError:
            # The final merge has not been setup yet
            pass

        for i in self._tempfiles:
            os.unlink(i)


# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
class BufferMerge(object):
    """
    A class for merging pre-sorted formatted GWAS files on chromosome,
    start_position and end_position. I would like to cythonise this but do
    not have time. Note that this can't handle quoted delimiters, it uses
    a simple split to process the rows. Also, all files that are given to it
    are opened at once. For more controlled behaviour use `MergeFiles`
    """
    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def __init__(self, files, keys, chunksize=100, delimiter="\t",
                 lineterminator="\n", open_method=gzip.open, header=True,
                 row_holder=None, as_bytes=False, **kwargs):
        """
        Parameters
        ----------
        files : str
            One or more file names. The first row of each file must be the
            header.
        keys : list of tuple
            The sort keys, each tuple should have column_name at [0],
            data_type at [1] and order at [2].
        chunksize : int, optional, default: 75
            The number of rows from each opened file that are buffered into
            memory.
        delimiter : str, optional, default: `\t`
            The delimiter of the input files (if un-opened). Note that so
            special allowance is given for quoted delimiters, so pass a
            csv.reader if you want them handled properly.
        lineterminator : str, optional, default: "\n"
            The line endings for each row
        open_method : :obj:`func`, optional, default: `gzip.open`
            The method to use to open the input files if strings are given.
            Note, this defaults to gzip as presumably the reason you are using
            an external merge sort is the files are big.
        header : bool, optional, default: True
            Do the input files have a header, if so, this is extracted and
            checked that the length is equivalent for each input file.
        **kwargs
            Keyword arguments to the open method
        """
        self._files = files
        self._keys = keys
        self._chunksize = chunksize

        # Will hold buffer counter objects that will contain the file object
        # for a single file and the number of rows that has been processed
        # from the file both in total and in the buffer
        self._counters = []

        # Will contain the buffered rows extracted from the files
        self.buffer = []

        # Will hold a list of files that have been processed (out of all the)
        # files passed to the merge object and those that have not been
        # processed. Which file ends up in which list depends on if it can be
        # opened or not. For example it might fail due to lack of inodes
        self.processed_files = []
        self.unprocessed_files = []

        # Make sure the delimiter is represented as bytes
        self._delimiter = delimiter
        self._lineterminator = lineterminator
        self._calls = 0
        self._open_method = open_method
        self._kwargs = kwargs
        self._has_header = header
        self._row_holder = row_holder
        self._as_bytes = as_bytes

        if self._as_bytes is True:
            self._open_mode = 'rb'
            self._delimiter = self._delimiter.encode()
            self._lineterminator = self._lineterminator.encode()
            if row_holder is None:
                self._row_holder = row_holder.BytesRowHolder
        else:
            self._open_mode = 'rt'
            if row_holder is None:
                self._row_holder = row_holder.StrRowHolder

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def __enter__(self):
        """
        Entry point for the context manager
        """
        self.open()
        return self

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def __exit__(self, *args):
        """
        Exit point for the context manager
        """
        self.close()

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def __iter__(self):
        """
        Initialise the iterator
        """
        return self

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def __next__(self):
        """
        Grab the next row, rows are returned in sort order of chr_name,
        start_pos, end_pos

        Returns
        -------
        row : list of int or bytes
            All the entries are assumed to be bytes apart from the
            start_pos and end_pos that are made into ints.
        """
        try:
            row = self.buffer.pop()
        except IndexError as e:
            # Nothing left in the buffer, so everything must have been used up
            # so we raise a StopIteration to signal termination of the Iterator
            raise StopIteration("no more rows") from e

        try:
            # Signal to the counter that we have removed a row. If the buffer
            # counter goes to 0 then a StopIteration is raised and that means
            # that we need to load up the next batch of rows from the file that
            # the buffer is attached to
            row.buffer_counter.remove_row()
        except StopIteration:
            try:
                # Try to load more rows
                self._init_buffer(row.buffer_counter)
                self._sort_method()
            except StopIteration:
                # TODO: Do not sort if nothing has been added
                self._sort_method()

                # TODO: close file
                row.buffer_counter.close()
            except ValueError as e:
                # There are no more rows left for that particular file
                # so we move on and return the row
                # IOError on closed file
                if e.args[0].lower() != "i/o operation on closed file.":
                    raise
        row = row.row()
        # if self._calls == 0:
        #     print("MergeFiles CALLS {0}".format(self._calls))
        #     print(row[:3])
        self._calls += 1
        # return [str(i) for i in row]
        return row

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def open(self):
        """
        Open all the files that need merging. Note that files that fail to open
        due to IOErrors do not cause fatal errors, rather they are added to an
        unprocessed list. This will only fail if < 2 files have been opened.
        The reason for this is that could run out of inodes if merging a lot
        of files and we do not want to crash the hold process but rather handle
        it.

        Raises
        ------
        IOError
            If less than 2 files are available to merge
        """
        header_len = -1
        header = None
        fobjs = []
        for i in self._files:
            try:
                fobj = self._open_method(i, self._open_mode, **self._kwargs)
                self.processed_files.append(i)
            except IOError:
                # File will not be processed
                self.unprocessed_files.append(i)
                raise

            if self._has_header is True:
                # Extract the header
                try:
                    header = next(fobj).strip(self._lineterminator).\
                        split(self._delimiter)
                except StopIteration as e:
                    # No data in file
                    raise StopIteration("no data in file") from e
                # Make sure all the headers are the same length
                # TODO: Should I check contents??
                if len(header) != header_len:
                    # -1 is the first iteration of the loop
                    if header_len != -1:
                        raise(ValueError("header lengths not equal"))

                # Update the header length
                header_len = len(header)

            # Add the opened file to the buffer counter, this keeps tabs on
            # the number of lines from the file that are loaded into memory
            # at one time
            fobjs.append(fobj)

        if len(self.processed_files) < 2:
            raise IOError("can't open enough files to merge")

        self.header_len = header_len
        self.header = header
        self._set_sort_method()
        # print(self._sort_method)
        for i in fobjs:
            self._counters.append(BufferCounter(
                i, self._final_keys, self._row_holder,
                delimiter=self._delimiter,
                lineterminator=self._lineterminator
            ))

        # Now initialise the buffer with content from each of the files that
        # has been loaded
        for i in self._counters:
            # TODO: Handle a StopIteration here
            self._init_buffer(i)
        self._sort_method()

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def _set_sort_method(self):
        """
        Depending on the keys we will adjust the sort method that we will use
        if the keys are mixed order, so some are ascending and some are
        descending, then we have to do sequential sorts from the last key to
        the first and rely on python's sort stability. If they are all the same
        direction then then we can do it in one go. Note that we sort on the
        opposite order that is specified by the keys as we take sorted rows
        from the end of the sorted list
        """
        # All True or all False
        if self._has_header is True:
            self._final_keys = [
                (self.header.index(i[0]), i[1], not i[2]) for i in self._keys
            ]
        else:
            self._final_keys = [(i[0], i[1], not i[2]) for i in self._keys]

        self._final_idx = []
        self._final_order = None

        key_order = sum([i[2] for i in self._keys])
        if key_order == 0 or key_order == len(self._keys):
            self._final_idx = tuple([i[0] for i in self._final_keys])
            self._final_order = self._final_keys[0][2]
            self._sort_method = self._sort
        else:
            # mixed ordering
            self._final_keys = list(reversed(self._final_keys))
            self._sort_method = self._sequential_sort

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def _init_buffer(self, counter):
        """
        Add rows to the buffer by extracting then from the file object
        associated with counter

        Parameters
        ----------
        counter : :obj`BufferCounter`
            A buffer counter that can count rows/out (and in)
        """
        for i in range(self._chunksize):
            self.buffer.append(counter.add_row())

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def _sort(self):
        """
        Sort the contents I sort in reverse as I am poping from the bottom of
        the list to return
        """
        self.buffer.sort(
            key=itemgetter(*self._final_idx),
            reverse=self._final_order
        )

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def _sequential_sort(self):
        """
        Sort the contents I sort in reverse as I am poping from the bottom of
        the list to return
        """
        for i in self._final_keys:
            self.buffer.sort(
                key=itemgetter(i[0]),
                reverse=i[2]
            )

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def close(self):
        """
        Close all the files
        """
        for i in self._counters:
            i.close()


# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
class HeapqMerge(object):
    """
    A class for merging pre-sorted formatted GWAS files on chromosome,
    start_position and end_position. I would like to cythonise this but do
    not have time. Note that this can't handle quoted delimiters, it uses
    a simple split to process the rows. Also, all files that are given to it
    are opened at once. For more controlled behaviour use `MergeFiles`
    """
    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def __init__(self, files, keys, delimiter="\t", lineterminator="\n",
                 open_method=gzip.open, header=True, reverse=False,
                 **kwargs):
        """
        Parameters
        ----------
        files : str
            One or more file names. The first row of each file must be the
            header.
        keys : list of tuple
            The sort keys, each tuple should have column_name at [0],
            data_type at [1] and order at [2].
        chunksize : int, optional, default: 75
            The number of rows from each opened file that are buffered into
            memory.
        delimiter : str, optional, default: `\t`
            The delimiter of the input files (if un-opened). Note that so
            special allowance is given for quoted delimiters, so pass a
            csv.reader if you want them handled properly.
        lineterminator : str, optional, default: "\n"
            The line endings for each row
        open_method : :obj:`func`, optional, default: `gzip.open`
            The method to use to open the input files if strings are given.
            Note, this defaults to gzip as presumably the reason you are using
            an external merge sort is the files are big.
        header : bool, optional, default: True
            Do the input files have a header, if so, this is extracted and
            checked that the length is equivalent for each input file.
        **kwargs
            Keyword arguments to the open method
        """
        self._files = files
        self._keys = keys
        self._reverse = reverse

        # Will hold a list of files that have been processed (out of all the)
        # files passed to the merge object and those that have not been
        # processed. Which file ends up in which list depends on if it can be
        # opened or not. For example it might fail due to lack of inodes
        self.processed_files = []
        self.unprocessed_files = []

        # Make sure the delimiter is represented as bytes
        self._delimiter = delimiter
        self._lineterminator = lineterminator
        self._open_method = open_method
        self._kwargs = kwargs
        self._has_header = header

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def __enter__(self):
        """
        Entry point for the context manager
        """
        self.open()
        return self

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def __exit__(self, *args):
        """
        Exit point for the context manager
        """
        self.close()

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def __iter__(self):
        """
        Initialise the iterator
        """
        return self

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def __next__(self):
        """
        Grab the next row, rows are returned in sort order of chr_name,
        start_pos, end_pos

        Returns
        -------
        row : list of int or bytes
            All the entries are assumed to be bytes apart from the
            start_pos and end_pos that are made into ints.
        """
        try:
            row = self.buffer.pop()
        except IndexError as e:
            # Nothing left in the buffer, so everything must have been used up
            # so we raise a StopIteration to signal termination of the Iterator
            raise StopIteration("no more rows") from e

        try:
            # Signal to the counter that we have removed a row. If the buffer
            # counter goes to 0 then a StopIteration is raised and that means
            # that we need to load up the next batch of rows from the file that
            # the buffer is attached to
            row.buffer_counter.remove_row()
        except StopIteration:
            try:
                # Try to load more rows
                self._init_buffer(row.buffer_counter)
                self._sort_method()
            except StopIteration:
                # TODO: Do not sort if nothing has been added
                self._sort_method()

                # TODO: close file
                row.buffer_counter.close()
            except ValueError as e:
                # There are no more rows left for that particular file
                # so we move on and return the row
                # IOError on closed file
                if e.args[0].lower() != "i/o operation on closed file.":
                    raise
        row = row.row()
        # if self._calls == 0:
        #     print("MergeFiles CALLS {0}".format(self._calls))
        #     print(row[:3])
        self._calls += 1
        # return [str(i) for i in row]
        return row

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def open(self):
        """
        Open all the files that need merging. Note that files that fail to open
        due to IOErrors do not cause fatal errors, rather they are added to an
        unprocessed list. This will only fail if < 2 files have been opened.
        The reason for this is that could run out of inodes if merging a lot
        of files and we do not want to crash the hold process but rather handle
        it.

        Raises
        ------
        IOError
            If less than 2 files are available to merge
        """
        header_len = -1
        header = None
        fobjs = []
        for i in self._files:
            try:
                fobj = self._open_method(i, self._open_mode, **self._kwargs)
                self.processed_files.append(i)
            except IOError:
                # File will not be processed
                self.unprocessed_files.append(i)
                raise

            if self._has_header is True:
                # Extract the header
                try:
                    header = next(fobj).strip(self._lineterminator).\
                        split(self._delimiter)
                except StopIteration as e:
                    # No data in file
                    raise StopIteration("no data in file") from e
                # Make sure all the headers are the same length
                # TODO: Should I check contents??
                if len(header) != header_len:
                    # -1 is the first iteration of the loop
                    if header_len != -1:
                        raise(ValueError("header lengths not equal"))

                # Update the header length
                header_len = len(header)

            # Add the opened file to the buffer counter, this keeps tabs on
            # the number of lines from the file that are loaded into memory
            # at one time
            fobjs.append(fobj)

        if len(self.processed_files) < 2:
            raise IOError("can't open enough files to merge")

        self.header_len = header_len
        self.header = header
        self._set_sort_method()
        # print(self._sort_method)
        for i in fobjs:
            self._counters.append(BufferCounter(
                i, self._final_keys, self._row_holder,
                delimiter=self._delimiter,
                lineterminator=self._lineterminator
            ))

        # Now initialise the buffer with content from each of the files that
        # has been loaded
        for i in self._counters:
            # TODO: Handle a StopIteration here
            self._init_buffer(i)
        self._sort_method()

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def _set_sort_method(self):
        """
        Depending on the keys we will adjust the sort method that we will use
        if the keys are mixed order, so some are ascending and some are
        descending, then we have to do sequential sorts from the last key to
        the first and rely on python's sort stability. If they are all the same
        direction then then we can do it in one go. Note that we sort on the
        opposite order that is specified by the keys as we take sorted rows
        from the end of the sorted list
        """
        # All True or all False
        if self._has_header is True:
            self._final_keys = [
                (self.header.index(i[0]), i[1], not i[2]) for i in self._keys
            ]
        else:
            self._final_keys = [(i[0], i[1], not i[2]) for i in self._keys]

        self._final_idx = []
        self._final_order = None

        key_order = sum([i[2] for i in self._keys])
        if key_order == 0 or key_order == len(self._keys):
            self._final_idx = tuple([i[0] for i in self._final_keys])
            self._final_order = self._final_keys[0][2]
            self._sort_method = self._sort
        else:
            # mixed ordering
            self._final_keys = list(reversed(self._final_keys))
            self._sort_method = self._sequential_sort

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def _init_buffer(self, counter):
        """
        Add rows to the buffer by extracting then from the file object
        associated with counter

        Parameters
        ----------
        counter : :obj`BufferCounter`
            A buffer counter that can count rows/out (and in)
        """
        for i in range(self._chunksize):
            self.buffer.append(counter.add_row())

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def _sort(self):
        """
        Sort the contents I sort in reverse as I am poping from the bottom of
        the list to return
        """
        self.buffer.sort(
            key=itemgetter(*self._final_idx),
            reverse=self._final_order
        )

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def _sequential_sort(self):
        """
        Sort the contents I sort in reverse as I am poping from the bottom of
        the list to return
        """
        for i in self._final_keys:
            self.buffer.sort(
                key=itemgetter(i[0]),
                reverse=i[2]
            )

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def close(self):
        """
        Close all the files
        """
        for i in self._counters:
            i.close()


# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
class BufferCounter(object):
    """
    Has the job of keeping count of the number of rows from the file that have
    been added and removed from the buffer
    """
    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def __init__(self, fobj, keys, row_holder, delimiter="\t",
                 lineterminator="\n"):
        """
        Parameters
        ----------
        delimiter : str, optional, default: "\t"
            The delimiter of the file. Note that no special handling of quoted
            or escaped delimiters occurs
        lineterminator : str, optional, default: "\n"
            The line endings that need striping
        """
        self.fobj = fobj
        self.keys = keys
        self._buffer_size = 0
        self._closed = False
        self._row_holder = row_holder

        # Make sure the delimiter is represented as bytes
        self._delimiter = delimiter
        self._lineterminator = lineterminator

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def add_row(self):
        """
        Both return a row and increment the counter for the file
        """
        row = next(self.fobj).strip(self._lineterminator).\
            split(self._delimiter)
        self._buffer_size += 1
        return self._row_holder(row, self)

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def close(self):
        """
        A wrapper for file.close that makes sure the file is not closed twice
        """
        if self._closed is False:
            self.fobj.close()

        self._closed = True

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def remove_row(self):
        """
        Decrement the row counter. If the counter reaches 0 a Stopiteration
        is raised
        """
        self._buffer_size -= 1

        if self._buffer_size == 0:
            raise StopIteration("need to rebuffer")
