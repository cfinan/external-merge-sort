"""Test the merging of chunk files
"""
from merge_sort import chunks, merge, examples
from pyaddons import utils
import common
import pytest
import math
import os
import csv
import pprint as pp


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def check_merge_str(all_rows, data, data_types, test_key, delimiter,
                    has_header=True):
    """Check that the merge sort rows are the same as the input data.

    Parameters
    ----------
    all_rows : `list` of `str`
        The rows returned by the merge sort, this is the data under test.
    data : `list` of `list`
        The source data to compare against.
    data_types : `list` of `str`
        The data types for the columns in ``data``. These are used to convert
        the data that is read in from a chunk file, so it can be compared to
        the source ``data``.
    test_key : `function`
        A sort key function that will be applied to slices of ``data``, should
        be the same as that used in the chunker except for not requiring any
        delimiter split.
    delimiter : `str`
        The delimiter used to join the rows prior to adding to the chunker.
    has_header : `bool`, optional, default: `True`
        Were the chunk files written with a header value.
    """
    if has_header is True:
        data.pop(0)
    data.sort(key=test_key)

    test_rows = []
    for row in all_rows:
        row = [
            examples._DTYPE_MAP[d](v) for d, v in
            zip(data_types, row.strip().split(delimiter))
        ]
        test_rows.append(row)

    assert len(test_rows) == len(data), "different row count"
    # assert test_rows == data, "wrong structure"
    idx = 0
    for i, j in zip(test_rows, data):
        assert i == j, f"wrong structure at: {idx}"
        idx += 1
    # pp.pprint()


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
@pytest.mark.parametrize(
    ("has_header,ncols,nrows,chunksize,key_dtypes,delimiter,chunk_suffix,"
     "write_method,use_chunker_class,use_merge_class,max_files"),
    [
        (True, 10, 14, 3, (str, str), "\t", ".chunk", open,
         chunks.SortedChunks, merge.IterativeHeapqMerge, 2),
        (True, 10, 57, 3, (str, str), "\t", ".chunk", open,
         chunks.SortedChunks, merge.IterativeHeapqMerge, 3),
        (True, 10, 57, 1, (str, str), "\t", ".chunk", open,
         chunks.SortedChunks, merge.IterativeHeapqMerge, 3),
        (True, 10, 57, 1, (str, str), "\t", ".chunk", open,
         chunks.SortedChunks, merge.IterativeHeapqMerge, 100),
        (True, 10, 14, 3, (int,), "\t", ".chunk", open,
         chunks.SortedChunks, merge.IterativeHeapqMerge, 2),
        (True, 10, 57, 3, (int,), "\t", ".chunk", open,
         chunks.SortedChunks, merge.IterativeHeapqMerge, 3),
        (True, 10, 57, 1, (int,), "\t", ".chunk", open,
         chunks.SortedChunks, merge.IterativeHeapqMerge, 3),
        (True, 10, 57, 1, (int,), "\t", ".chunk", open,
         chunks.SortedChunks, merge.IterativeHeapqMerge, 100),
        (True, 10, 14, 3, (float, float), "\t", ".chunk", open,
         chunks.SortedChunks, merge.IterativeHeapqMerge, 2),
        (True, 10, 57, 3, (float, float), "\t", ".chunk", open,
         chunks.SortedChunks, merge.IterativeHeapqMerge, 3),
        (True, 10, 57, 1, (float, float), "\t", ".chunk", open,
         chunks.SortedChunks, merge.IterativeHeapqMerge, 3),
        (True, 10, 57, 1, (float, float), "\t", ".chunk", open,
         chunks.SortedChunks, merge.IterativeHeapqMerge, 100),
        (False, 10, 14, 3, (str, str), "\t", ".chunk", open,
         chunks.SortedChunks, merge.IterativeHeapqMerge, 2),
        (False, 10, 57, 3, (str, str), "\t", ".chunk", open,
         chunks.SortedChunks, merge.IterativeHeapqMerge, 3),
        (False, 10, 57, 1, (str, str), "\t", ".chunk", open,
         chunks.SortedChunks, merge.IterativeHeapqMerge, 3),
        (False, 10, 57, 1, (str, str), "\t", ".chunk", open,
         chunks.SortedChunks, merge.IterativeHeapqMerge, 100),
        (False, 10, 14, 3, (int,), "\t", ".chunk", open,
         chunks.SortedChunks, merge.IterativeHeapqMerge, 2),
        (False, 10, 57, 3, (int,), "\t", ".chunk", open,
         chunks.SortedChunks, merge.IterativeHeapqMerge, 3),
        (False, 10, 57, 1, (int,), "\t", ".chunk", open,
         chunks.SortedChunks, merge.IterativeHeapqMerge, 3),
        (False, 10, 57, 1, (int,), "\t", ".chunk", open,
         chunks.SortedChunks, merge.IterativeHeapqMerge, 100),
        (False, 10, 14, 3, (float, float), "\t", ".chunk", open,
         chunks.SortedChunks, merge.IterativeHeapqMerge, 2),
        (False, 10, 57, 3, (float, float), "\t", ".chunk", open,
         chunks.SortedChunks, merge.IterativeHeapqMerge, 3),
        (False, 10, 57, 1, (float, float), "\t", ".chunk", open,
         chunks.SortedChunks, merge.IterativeHeapqMerge, 3),
        (False, 10, 57, 1, (float, float), "\t", ".chunk", open,
         chunks.SortedChunks, merge.IterativeHeapqMerge, 100),
    ]
)
def test_sorted_chunk_merge(tmpdir, has_header, ncols, nrows, chunksize,
                            key_dtypes, delimiter, chunk_suffix, write_method,
                            use_chunker_class, use_merge_class, max_files):
    """Tests for the ``merge_sort.merge.IterativeHeapqMerge`` and
    ``merge_sort.chunks.SortedChunks``.
    """
    # Get test data and data types that are in each column
    data, data_types = examples.create_data(has_header=has_header, ncols=ncols,
                                            nrows=nrows)
    # Get the sort key that will be given to the chunker
    sort_key, key_cols = common.get_list_sort_func(data_types, key_dtypes,
                                                   delimiter=delimiter)
    # Get the sort key that will be used on the test data (does not require
    # delimiter splitting in the sort key)
    test_key, key_cols = common.get_list_sort_func(data_types, key_dtypes,
                                                   delimiter=None)

    # These conditions must be met for the test to be valid
    assert data == data, "invalid test"

    start_idx = 0
    header = None
    if has_header is True:
        start_idx = 1
        header = delimiter.join(data[0]) + "\n"

    # Initialise the chunker and add the rows
    chunker = use_chunker_class(
        tmpdir, key=sort_key, chunksize=chunksize,
        header=header, chunk_suffix=chunk_suffix,
        write_method=write_method
    )

    with chunker as c:
        for row in data[start_idx:]:
            c.add_row(
                "{0}\n".format(delimiter.join([str(i) for i in row]))
            )

    # glob the directory and get the chunk files that have been output
    glob_files = common.get_chunk_files(tmpdir, suffix=chunk_suffix)

    # Make sure we have the expected number of rows
    assert len(glob_files) == math.ceil(
        (len(data) - int(has_header)) / chunksize
    ), "wrong file number"

    # Make sure we have output all the files that the chunker thinks it has
    # output
    chunk_files = c.chunk_file_list
    assert sorted(chunk_files) == sorted(glob_files), "wrong files"

    # Now check that all the files have the expected content
    # _check_chunk_files(chunk_files, data, data_types, test_key, chunksize,
    #                    delimiter, write_method, has_header=has_header)
    all_rows = []
    with use_merge_class(c.chunk_file_list, key=sort_key, max_files=max_files,
                         tmpdir=tmpdir, read_method=open, write_method=open,
                         header=has_header, check_header=True) as m:
        for row in m:
            all_rows.append(row)

    check_merge_str(all_rows, data, data_types, test_key, delimiter,
                    has_header=has_header)


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def check_merge_list(all_rows, data, data_types, test_key, has_header=True):
    """Check that the merge sort rows are the same as the input data.

    Parameters
    ----------
    all_rows : `list` of `str`
        The rows returned by the merge sort, this is the data under test.
    data : `list` of `list`
        The source data to compare against.
    data_types : `list` of `str`
        The data types for the columns in ``data``. These are used to convert
        the data that is read in from a chunk file, so it can be compared to
        the source ``data``.
    test_key : `function`
        A sort key function that will be applied to slices of ``data``, should
        be the same as that used in the chunker except for not requiring any
        delimiter split.
    has_header : `bool`, optional, default: `True`
        Were the chunk files written with a header value.
    """
    if has_header is True:
        data.pop(0)
    data.sort(key=test_key)

    test_rows = []
    for row in all_rows:
        row = [examples._DTYPE_MAP[d](v) for d, v in zip(data_types, row)]
        test_rows.append(row)

    assert len(test_rows) == len(data), "different row count"
    assert test_rows == data, "wrong structure"


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
@pytest.mark.parametrize(
    ("has_header,ncols,nrows,chunksize,key_dtypes,delimiter,chunk_suffix,"
     "write_method,use_chunker_class,use_merge_class,max_files"),
    [
        (True, 10, 14, 3, (str, str), "\t", ".chunk", open,
         chunks.CsvSortedChunks, merge.CsvIterativeHeapqMerge, 2),
        (True, 10, 57, 3, (str, str), "\t", ".chunk", open,
         chunks.CsvSortedChunks, merge.CsvIterativeHeapqMerge, 3),
        (True, 10, 57, 1, (str, str), "\t", ".chunk", open,
         chunks.CsvSortedChunks, merge.CsvIterativeHeapqMerge, 3),
        (True, 10, 57, 1, (str, str), "\t", ".chunk", open,
         chunks.CsvSortedChunks, merge.CsvIterativeHeapqMerge, 100),
        (True, 10, 14, 3, (int,), "\t", ".chunk", open,
         chunks.CsvSortedChunks, merge.CsvIterativeHeapqMerge, 2),
        (True, 10, 57, 3, (int,), "\t", ".chunk", open,
         chunks.CsvSortedChunks, merge.CsvIterativeHeapqMerge, 3),
        (True, 10, 57, 1, (int,), "\t", ".chunk", open,
         chunks.CsvSortedChunks, merge.CsvIterativeHeapqMerge, 3),
        (True, 10, 57, 1, (int,), "\t", ".chunk", open,
         chunks.CsvSortedChunks, merge.CsvIterativeHeapqMerge, 100),
        (True, 10, 14, 3, (float, float), "\t", ".chunk", open,
         chunks.CsvSortedChunks, merge.CsvIterativeHeapqMerge, 2),
        (True, 10, 57, 3, (float, float), "\t", ".chunk", open,
         chunks.CsvSortedChunks, merge.CsvIterativeHeapqMerge, 3),
        (True, 10, 57, 1, (float, float), "\t", ".chunk", open,
         chunks.CsvSortedChunks, merge.CsvIterativeHeapqMerge, 3),
        (True, 10, 57, 1, (float, float), "\t", ".chunk", open,
         chunks.CsvSortedChunks, merge.CsvIterativeHeapqMerge, 100),
        (False, 10, 14, 3, (str, str), "\t", ".chunk", open,
         chunks.CsvSortedChunks, merge.CsvIterativeHeapqMerge, 2),
        (False, 10, 57, 3, (str, str), "\t", ".chunk", open,
         chunks.CsvSortedChunks, merge.CsvIterativeHeapqMerge, 3),
        (False, 10, 57, 1, (str, str), "\t", ".chunk", open,
         chunks.CsvSortedChunks, merge.CsvIterativeHeapqMerge, 3),
        (False, 10, 57, 1, (str, str), "\t", ".chunk", open,
         chunks.CsvSortedChunks, merge.CsvIterativeHeapqMerge, 100),
        (False, 10, 14, 3, (int,), "\t", ".chunk", open,
         chunks.CsvSortedChunks, merge.CsvIterativeHeapqMerge, 2),
        (False, 10, 57, 3, (int,), "\t", ".chunk", open,
         chunks.CsvSortedChunks, merge.CsvIterativeHeapqMerge, 3),
        (False, 10, 57, 1, (int,), "\t", ".chunk", open,
         chunks.CsvSortedChunks, merge.CsvIterativeHeapqMerge, 3),
        (False, 10, 57, 1, (int,), "\t", ".chunk", open,
         chunks.CsvSortedChunks, merge.CsvIterativeHeapqMerge, 100),
        (False, 10, 14, 3, (float, float), "\t", ".chunk", open,
         chunks.CsvSortedChunks, merge.CsvIterativeHeapqMerge, 2),
        (False, 10, 57, 3, (float, float), "\t", ".chunk", open,
         chunks.CsvSortedChunks, merge.CsvIterativeHeapqMerge, 3),
        (False, 10, 57, 1, (float, float), "\t", ".chunk", open,
         chunks.CsvSortedChunks, merge.CsvIterativeHeapqMerge, 3),
        (False, 10, 57, 1, (float, float), "\t", ".chunk", open,
         chunks.CsvSortedChunks, merge.CsvIterativeHeapqMerge, 100),
    ]
)
def test_csv_sorted_chunk_merge(tmpdir, has_header, ncols, nrows, chunksize,
                                key_dtypes, delimiter, chunk_suffix,
                                write_method, use_chunker_class,
                                use_merge_class, max_files):
    """Tests for the ``merge_sort.merge.CsvIterativeHeapqMerge`` and
    ``merge_sort.chunks.CsvSortedChunks``.
    """
    # Get test data and data types that are in each column
    data, data_types = examples.create_data(has_header=has_header, ncols=ncols,
                                            nrows=nrows)
    # Get the sort key that will be used on the test data (does not require
    # delimiter splitting in the sort key)
    sort_key, key_cols = common.get_list_sort_func(data_types, key_dtypes,
                                                   delimiter=None)

    # These conditions must be met for the test to be valid
    assert data == data, "invalid test"

    start_idx = 0
    header = None
    if has_header is True:
        start_idx = 1
        header = data[0]

    # Initialise the chunker and add the rows
    chunker = use_chunker_class(
        tmpdir, key=sort_key, chunksize=chunksize,
        header=header, chunk_suffix=chunk_suffix,
        write_method=write_method, delimiter=delimiter,
        lineterminator=os.linesep
    )

    with chunker as c:
        for row in data[start_idx:]:
            c.add_row(row)

    # glob the directory and get the chunk files that have been output
    glob_files = common.get_chunk_files(tmpdir, suffix=chunk_suffix)

    # Make sure we have the expected number of rows
    assert len(glob_files) == math.ceil(
        (len(data) - int(has_header)) / chunksize
    ), "wrong file number"

    # Make sure we have output all the files that the chunker thinks it has
    # output
    chunk_files = c.chunk_file_list
    assert sorted(chunk_files) == sorted(glob_files), "wrong files"

    # Now check that all the files have the expected content
    all_rows = []
    with use_merge_class(c.chunk_file_list, key=sort_key, max_files=max_files,
                         tmpdir=tmpdir, read_method=open, write_method=open,
                         header=has_header, check_header=True,
                         csv_kwargs=dict(delimiter=delimiter,
                                         lineterminator=os.linesep)) as m:
        for row in m:
            all_rows.append(row)

    check_merge_list(all_rows, data, data_types, sort_key,
                     has_header=has_header)


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def check_merge_dict(all_rows, data, data_types, test_key, has_header=True):
    """Check that the merge sort rows are the same as the input data.

    Parameters
    ----------
    all_rows : `list` of `str`
        The rows returned by the merge sort, this is the data under test.
    data : `list` of `list`
        The source data to compare against.
    data_types : `list` of `str`
        The data types for the columns in ``data``. These are used to convert
        the data that is read in from a chunk file, so it can be compared to
        the source ``data``.
    test_key : `function`
        A sort key function that will be applied to slices of ``data``, should
        be the same as that used in the chunker except for not requiring any
        delimiter split.
    has_header : `bool`, optional, default: `True`
        Were the chunk files written with a header value.
    """
    if has_header is True:
        data.pop(0)
    data.sort(key=test_key)

    test_rows = []
    for row in all_rows:
        row = dict(
            [(k, examples._DTYPE_MAP[d](v))
             for k, d, v in
             zip(data_types.keys(), data_types.values(), row.values())]
        )
        test_rows.append(row)

    assert len(test_rows) == len(data), "different row count"
    assert test_rows == data, "wrong structure"


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
@pytest.mark.parametrize(
    ("has_header,ncols,nrows,chunksize,key_dtypes,delimiter,chunk_suffix,"
     "write_method,use_chunker_class,use_merge_class,max_files"),
    [
        (True, 10, 14, 3, (str, str), "\t", ".chunk", open,
         chunks.CsvDictSortedChunks, merge.CsvDictIterativeHeapqMerge, 2),
        (True, 10, 57, 3, (str, str), "\t", ".chunk", open,
         chunks.CsvDictSortedChunks, merge.CsvDictIterativeHeapqMerge, 3),
        (True, 10, 57, 1, (str, str), "\t", ".chunk", open,
         chunks.CsvDictSortedChunks, merge.CsvDictIterativeHeapqMerge, 3),
        (True, 10, 57, 1, (str, str), "\t", ".chunk", open,
         chunks.CsvDictSortedChunks, merge.CsvDictIterativeHeapqMerge, 100),
        (True, 10, 14, 3, (int,), "\t", ".chunk", open,
         chunks.CsvDictSortedChunks, merge.CsvDictIterativeHeapqMerge, 2),
        (True, 10, 57, 3, (int,), "\t", ".chunk", open,
         chunks.CsvDictSortedChunks, merge.CsvDictIterativeHeapqMerge, 3),
        (True, 10, 57, 1, (int,), "\t", ".chunk", open,
         chunks.CsvDictSortedChunks, merge.CsvDictIterativeHeapqMerge, 3),
        (True, 10, 57, 1, (int,), "\t", ".chunk", open,
         chunks.CsvDictSortedChunks, merge.CsvDictIterativeHeapqMerge, 100),
        (True, 10, 14, 3, (float, float), "\t", ".chunk", open,
         chunks.CsvDictSortedChunks, merge.CsvDictIterativeHeapqMerge, 2),
        (True, 10, 57, 3, (float, float), "\t", ".chunk", open,
         chunks.CsvDictSortedChunks, merge.CsvDictIterativeHeapqMerge, 3),
        (True, 10, 57, 1, (float, float), "\t", ".chunk", open,
         chunks.CsvDictSortedChunks, merge.CsvDictIterativeHeapqMerge, 3),
        (True, 10, 57, 1, (float, float), "\t", ".chunk", open,
         chunks.CsvDictSortedChunks, merge.CsvDictIterativeHeapqMerge, 100),
    ]
)
def test_csv_dict_sorted_chunk_merge(tmpdir, has_header, ncols, nrows,
                                     chunksize, key_dtypes, delimiter,
                                     chunk_suffix, write_method,
                                     use_chunker_class, use_merge_class,
                                     max_files):
    """Tests for the ``merge.CsvDictIterativeHeapqMerge`` and
    ``merge_sort.chunks.CsvDictSortedChunks``. This applies the chunkers to
    some test data and ensures the expected chunk files are created.
    """
    # Get test data and data types that are in each column
    data, data_types = examples.create_data(has_header=has_header, ncols=ncols,
                                            nrows=nrows, use_dict=True)
    # Get the sort key that will be used on the test data (does not require
    # delimiter splitting in the sort key)
    sort_key, key_cols = common.get_dict_sort_func(data_types, key_dtypes)

    # These conditions must be met for the test to be valid
    assert data == data, "invalid test"

    start_idx = 0
    header = None
    if has_header is True:
        start_idx = 1
        header = data[0]

    # Initialise the chunker and add the rows
    chunker = use_chunker_class(
        tmpdir, key=sort_key, chunksize=chunksize,
        header=header, chunk_suffix=chunk_suffix,
        write_method=write_method, delimiter=delimiter,
        lineterminator=os.linesep
    )

    with chunker as c:
        for row in data[start_idx:]:
            c.add_row(row)

    # glob the directory and get the chunk files that have been output
    glob_files = common.get_chunk_files(tmpdir, suffix=chunk_suffix)

    # Make sure we have the expected number of rows
    assert len(glob_files) == math.ceil(
        (len(data) - int(has_header)) / chunksize
    ), "wrong file number"

    # Make sure we have output all the files that the chunker thinks it has
    # output
    chunk_files = c.chunk_file_list

    assert sorted(chunk_files) == sorted(glob_files), "wrong files"

    all_rows = []
    with use_merge_class(c.chunk_file_list, key=sort_key, max_files=max_files,
                         tmpdir=tmpdir, read_method=open, write_method=open,
                         header=has_header, check_header=True,
                         csv_kwargs=dict(delimiter=delimiter,
                                         lineterminator=os.linesep)) as m:
        for row in m:
            all_rows.append(row)

    check_merge_dict(all_rows, data, data_types, sort_key,
                     has_header=has_header)


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
@pytest.mark.parametrize(
    ("has_header,ncols,nrows,nfiles,comment_lines,comment_char,key_dtypes,"
     "delimiter,write_method,use_merge_class,max_files"),
    [
        (True, 10, 10, 3, 10, "##", (str, str), "\t", open,
         merge.IterativeHeapqMerge, 2),
        (True, 10, 107, 10, 101, "##", (str, str), "\t", open,
         merge.IterativeHeapqMerge, 2),
    ]
)
def test_comments_sort(tmpdir, has_header, ncols, nrows, nfiles,
                       comment_lines, comment_char, key_dtypes,
                       delimiter, write_method, use_merge_class,
                       max_files):
    """Tests for the ``merge_sort.merge.IterativeHeapqMerge`` and
    ``merge_sort.chunks.SortedChunks``.
    """
    # Get test data and data types that are in each column
    data, data_types = examples.create_data(
        has_header=has_header, ncols=ncols, nrows=nrows, comment=comment_char,
        comment_lines=comment_lines
    )
    # Get the sort key that will be given to the chunker
    sort_key, key_cols = common.get_list_sort_func(data_types, key_dtypes,
                                                   delimiter=delimiter)
    # Get the sort key that will be used on the test data (does not require
    # delimiter splitting in the sort key)
    test_key, key_cols = common.get_list_sort_func(data_types, key_dtypes,
                                                   delimiter=None)

    # These conditions must be met for the test to be valid
    assert data == data, "invalid test"
    merge_files = _write_data(
        tmpdir, data, test_key, has_header, comment_lines,
        delimiter=delimiter, open_method=write_method, nfiles=nfiles
    )

    all_rows = []
    with use_merge_class(merge_files, key=sort_key, max_files=max_files,
                         tmpdir=tmpdir, read_method=write_method,
                         write_method=write_method, header=has_header,
                         check_header=True, comment=comment_char) as m:
        for row in m:
            all_rows.append(row)

    lead_lines = comment_lines
    check_merge_str(all_rows, data[lead_lines:], data_types, test_key,
                    delimiter, has_header=has_header)


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
@pytest.mark.parametrize(
    ("has_header,ncols,nrows,nfiles,comment_lines,comment_char,key_dtypes,"
     "delimiter,write_method,use_merge_class,max_files,use_dict"),
    [
        (True, 10, 10, 3, 10, "##", (str, str), "\t", open,
         merge.CsvIterativeHeapqMerge, 2, False),
        (True, 10, 107, 10, 101, "##", (str, str), "\t", open,
         merge.CsvIterativeHeapqMerge, 2, False),
        (True, 10, 10, 3, 10, "##", (str, str), "\t", open,
         merge.CsvDictIterativeHeapqMerge, 2, True),
        (True, 10, 107, 10, 101, "##", (str, str), "\t", open,
         merge.CsvDictIterativeHeapqMerge, 2, True),
    ]
)
def test_comments_csv(tmpdir, has_header, ncols, nrows, nfiles,
                      comment_lines, comment_char, key_dtypes,
                      delimiter, write_method, use_merge_class,
                      max_files, use_dict):
    """Tests for the ``merge_sort.merge.IterativeHeapqMerge`` and
    ``merge_sort.chunks.SortedChunks``.
    """
    # Get test data and data types that are in each column
    data, data_types = examples.create_data(
        has_header=has_header, ncols=ncols, nrows=nrows, comment=comment_char,
        comment_lines=comment_lines, use_dict=use_dict
    )

    # Get the sort key that will be given to the chunker
    sort_key, key_cols = None, None
    if use_dict is False:
        sort_key, key_cols = common.get_list_sort_func(data_types, key_dtypes)
    else:
        sort_key, key_cols = common.get_dict_sort_func(data_types, key_dtypes)

    # These conditions must be met for the test to be valid
    assert data == data, "invalid test"
    merge_files = _write_data(
        tmpdir, data, sort_key, has_header, comment_lines,
        delimiter=delimiter, open_method=write_method, nfiles=nfiles,
        use_dict=use_dict
    )

    all_rows = []
    with use_merge_class(merge_files, key=sort_key, max_files=max_files,
                         tmpdir=tmpdir, read_method=write_method,
                         write_method=write_method, header=has_header,
                         check_header=True, comment=comment_char,
                         csv_kwargs=dict(delimiter=delimiter,
                                         lineterminator=os.linesep)) as m:
        for row in m:
            all_rows.append(row)

    lead_lines = comment_lines

    if use_dict is False:
        check_merge_list(all_rows, data[lead_lines:], data_types, sort_key,
                         has_header=has_header)
    else:
        check_merge_dict(all_rows, data[lead_lines:], data_types, sort_key,
                         has_header=has_header)


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def _write_data(tmpdir, data, sort_key, has_header, comment_lines, nfiles=3,
                open_method=open, delimiter="\t", use_dict=False):
    """
    """
    lead_lines = comment_lines + has_header
    data_rows = data[lead_lines:]
    step_size = math.ceil(len(data_rows) / nfiles)
    merge_files = []
    nwritten = 0
    for i in range(0, len(data_rows), step_size):
        outfn = utils.get_temp_file(dir=tmpdir)
        with open_method(outfn, 'wt') as outf:
            for j in data[:lead_lines]:
                outf.write("{0}\n".format(delimiter.join(j)))

            writer = None
            if use_dict is False:
                writer = csv.writer(outf, delimiter=delimiter)
            else:
                writer = csv.DictWriter(
                    outf, list(data_rows[0].keys()), delimiter=delimiter
                )

            for j in sorted(data_rows[i:i+step_size], key=sort_key):
                nwritten += 1
                writer.writerow(j)

        merge_files.append(outfn)
    assert nwritten == len(data_rows)
    return merge_files
