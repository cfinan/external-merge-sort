"""Tests for the ``merge_sort.sort`` module.
"""
from merge_sort import examples
import merge_sort
import pytest
import common
import pprint as pp


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
@pytest.mark.parametrize(
    ("test_data,key_func,reverse,chunksize"),
    [
        ([examples.get_integer() for i in range(100)], lambda x: int(x),
         False, 3),
        ([examples.get_integer(seed=i) for i in range(100)], lambda x: int(x),
         False, 3),
        ([examples.get_integer(seed=i) for i in range(100)], lambda x: int(x),
         True, 3),
        ([examples.get_float(seed=i) for i in range(100)], lambda x: float(x),
         False, 3),
        ([examples.get_float(seed=i) for i in range(100)], lambda x: float(x),
         True, 3),
        ([examples.get_string(seed=i) for i in range(100)], None,
         False, 3),
        ([examples.get_string(seed=i) for i in range(100)], None,
         True, 3),
    ]
)
def test_sorted_simple(tmpdir, test_data, key_func, reverse, chunksize):
    """Tests for ``merge_sort.sort.sorted`.
    """
    # pp.pprint(test_data)
    sortgen = merge_sort.sorted(test_data, key=key_func, reverse=reverse,
                                chunksize=chunksize, tmpdir=tmpdir)

    result = []
    if key_func is not None:
        result = [key_func(i) for i in sortgen]
    else:
        result = [i.strip() for i in sortgen]

    test_data.sort(key=key_func, reverse=reverse)
    assert test_data == result, "wrong data"


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
@pytest.mark.parametrize(
    ("has_header,reverse,ncols,nrows,chunksize,key_dtypes"),
    [
        (True, False, 10, 100, 3, (str, str)),
        (True, True, 10, 100, 3, (str, str)),
        (True, False, 10, 100, 3, (str, int)),
        (True, True, 10, 100, 3, (str, int)),
        (True, False, 10, 100, 3, (str, float)),
        (True, True, 10, 100, 3, (str, float)),
        (True, False, 10, 100, 3, (int, int)),
        (True, True, 10, 100, 3, (int, int)),
        (True, False, 10, 100, 3, (int, str)),
        (True, True, 10, 100, 3, (int, str)),
        (True, False, 10, 100, 3, (int, float)),
        (True, True, 10, 100, 3, (int, float)),
        (True, False, 10, 100, 3, (float, float)),
        (True, True, 10, 100, 3, (float, float)),
        (True, False, 10, 100, 3, (float, str)),
        (True, True, 10, 100, 3, (float, str)),
        (True, False, 10, 100, 3, (float, int)),
        (True, True, 10, 100, 3, (float, int)),
    ]
)
def test_sorted_list(tmpdir, has_header, reverse, ncols, nrows, chunksize,
                     key_dtypes):
    """Tests for ``merge_sort.sort.sorted` using lists as input.
    """
    # Get test data and data types that are in each column
    data, data_types = examples.create_data(has_header=has_header, ncols=ncols,
                                            nrows=nrows)

    # Get the sort key that will be used on the test data (does not require
    # delimiter splitting in the sort key)
    sort_key, key_cols = common.get_list_sort_func(data_types, key_dtypes,
                                                   delimiter=None)

    if has_header is True:
        header = data.pop(0)

    result = []
    for row in  merge_sort.sorted(data, key=sort_key, reverse=reverse,
                                  chunksize=chunksize, tmpdir=tmpdir):
        result.append(examples.set_row_type(row, data_types))

    data.sort(key=sort_key, reverse=reverse)
    assert data == result, "wrong result"


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
@pytest.mark.parametrize(
    ("has_header,reverse,ncols,nrows,chunksize,key_dtypes"),
    [
        (True, False, 10, 100, 3, (str, str)),
        (True, True, 10, 100, 3, (str, str)),
        (True, False, 10, 100, 3, (str, int)),
        (True, True, 10, 100, 3, (str, int)),
        (True, False, 10, 100, 3, (str, float)),
        (True, True, 10, 100, 3, (str, float)),
        (True, False, 10, 100, 3, (int, int)),
        (True, True, 10, 100, 3, (int, int)),
        (True, False, 10, 100, 3, (int, str)),
        (True, True, 10, 100, 3, (int, str)),
        (True, False, 10, 100, 3, (int, float)),
        (True, True, 10, 100, 3, (int, float)),
        (True, False, 10, 100, 3, (float, float)),
        (True, True, 10, 100, 3, (float, float)),
        (True, False, 10, 100, 3, (float, str)),
        (True, True, 10, 100, 3, (float, str)),
        (True, False, 10, 100, 3, (float, int)),
        (True, True, 10, 100, 3, (float, int)),
    ]
)
def test_sorted_dict(tmpdir, has_header, reverse, ncols, nrows, chunksize,
                     key_dtypes):
    """Tests for ``merge_sort.sort.sorted` using lists as input.
    """
    # Get test data and data types that are in each column
    data, data_types = examples.create_data(has_header=has_header, ncols=ncols,
                                            nrows=nrows, use_dict=True)

    # Get the sort key that will be used on the test data (does not require
    # delimiter splitting in the sort key)
    sort_key, key_cols = common.get_dict_sort_func(data_types, key_dtypes)

    if has_header is True:
        header = data.pop(0)

    result = []
    for row in  merge_sort.sorted(data, key=sort_key, reverse=reverse,
                                  chunksize=chunksize, tmpdir=tmpdir):
        result.append(examples.set_row_type(row, list(data_types.values())))

    data.sort(key=sort_key, reverse=reverse)
    assert data == result, "wrong result"
