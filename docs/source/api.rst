==============
merge-sort API
==============

Below is the API documentation for merge-sort. It is broadly partitioned into chunk sub-setting, merging of sorted chunks and file joins.

``merge_sort.sort``
-------------------

.. automodule:: merge_sort.sort
   :members:
   :undoc-members:
   :show-inheritance:

``merge_sort.chunks``
---------------------

.. automodule:: merge_sort.chunks
   :members:
   :undoc-members:
   :show-inheritance:

``merge_sort.merge``
--------------------

.. automodule:: merge_sort.merge
   :members:
   :undoc-members:
   :show-inheritance:

``merge_sort.common``
---------------------

.. automodule:: merge_sort.common
   :members:
   :undoc-members:
   :show-inheritance:
